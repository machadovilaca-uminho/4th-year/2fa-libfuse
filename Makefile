clean:
	docker-compose stop

all:
	docker-compose build
	docker-compose up -d
	docker exec -it 2fa-libfuse_libfuse_1 bash
