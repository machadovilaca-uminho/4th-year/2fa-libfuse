# 2fa-libfuse

## Starting up (Docker)

To start the development environment just run `make all`.

This will launch 2 docker containers, one running the `webserver` and the other running an operative system with a libfuse mounted partition.

After that, the Makefile will automatically open a bash to the libfuse filesystem.

## Two-Factor Authentication (2FA)

To allow access to new users, the webserver has a code generator endpoint, `http://localhost:8000/generate_secret`, which gives a secret code and a QR code. One of those will be used to generate the 2FA codes in an application like [google-authenticator](https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2).

The user id and the generated secret code must also be added to the [users file](webserver/users.txt).

## Usage

Whenever you try to access a file in the mounted directory, using, for instance, the `cat` command, the libfuse implementation will connect to the webserver verifying if the user is authorized to open files.

If not, he will be requested to enter his two factor authentication code generated by the app, for example, at `http://localhost:8000/100`.

When the user inserts his code correctly, he will have 30 seconds to open the file.
