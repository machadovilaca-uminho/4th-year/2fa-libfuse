extern crate run_script;

use run_script::ScriptOptions;
use std::os::raw::{c_int, c_char};
use std::ffi::CStr;
use std::fs::OpenOptions;
use std::io::Write;
use std::collections::HashMap;
use reqwest::Error;


#[no_mangle]
pub extern "C" fn auth_verification(path: *const c_char, uid: *const c_char) -> c_int {
    let p = unsafe { CStr::from_ptr(path).to_string_lossy().into_owned() };
    let u = unsafe { CStr::from_ptr(uid).to_string_lossy().into_owned() };

    let verify = has_access(u.clone());
    let has_access;

    if verify.is_ok() {
        has_access = verify.unwrap();
    } else {
        has_access = false;
    }

    log(has_access, u, p);

    return if has_access { 0 } else { 1 };
}


//Metodo que faz o pedido ao server 
//verifica se o pedido esta ok
//retorna a resposta consoante o valor que o server lhe passa -> true || false
fn has_access(uid: String) -> Result<bool, Error> {
    let options = ScriptOptions::new();
    let args = vec![];
    let (_code, _output, _error) = run_script::run(
        format!("for pts in /dev/pts/*;do echo \"You have 30 seconds to insert your 2-factor authentication at http://localhost:8000/{}\" > $pts;done;", "100").as_ref(),
        &args, &options
    ).unwrap();

    let mut map = HashMap::new();
    map.insert("uid", uid.clone());
    map.insert("api_key", "aeS5ieT5hieYoteeVo5Xoo3chahceik9".parse().unwrap());

    let client = reqwest::blocking::Client::new();

    let resp: Result<String, Error> = client.post("http://webserver:8000/has_access")
        .json(&map)
        .send()?
        .text();

    return if resp.unwrap() == "true" {
        Ok(true)
    } else {
        Ok(false)
    }
}

//ficheiro de logs que descreve o controlo do sistema
fn log(has_access: bool, uid: String, path: String) {
    let mut f = OpenOptions::new().write(true).open("/tmp/log").unwrap();
    f.write_all("User ".as_bytes()).expect("Unable to write data");
    f.write_all(uid.as_bytes()).expect("Unable to write data");

    if has_access {
        f.write_all(" opened ".as_bytes()).expect("Unable to write data");
    } else {
        f.write_all(" failed to open ".as_bytes()).expect("Unable to write data");
    };

    f.write_all(path.as_bytes()).expect("Unable to write data");
    f.write_all("\n".as_bytes()).expect("Unable to write data");
}
