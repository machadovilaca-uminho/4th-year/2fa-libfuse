#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use] extern crate rocket;
extern crate rocket_contrib;
extern crate serde;
extern crate google_authenticator;
extern crate eventual;

use rocket_contrib::templates::Template;
use rocket_contrib::json::Json;
use serde::Deserialize;
use std::fs::File;
use std::io::{BufReader, BufRead};
use eventual::Timer;
use rocket::State;
use std::sync::Mutex;
use std::borrow::Borrow;
use std::collections::HashMap;
use rocket::request::LenientForm;
use rocket::fairing::AdHoc;


#[derive(Deserialize)]
#[derive(FromForm)]
struct Request {
    uid: String,
    api_key: String
}


struct Allowed {
    users: Mutex<Vec<String>>,
}


struct PermissionFile(String);

// Método que verifica se o user tem permisao para aceder ao ficheiro
//tem o ticker durante 30 segundos
fn has_access_to(request: Json<Request>, allowed: State<Allowed>) -> &'static str {
    let timer = Timer::new();
    let ticks = timer.interval_ms(1000).iter();
    let mut i = 0;

    allowed.users.lock().unwrap().retain(|x| x != request.uid.as_str());

    for _ in ticks {
        i = i+1;

        if allowed.users.lock().unwrap().contains(request.uid.borrow()) {
            allowed.users.lock().unwrap().retain(|x| x != request.uid.as_str());
            return "true";
        }

        if i > 30 {
            return "false";
        }
    }

    "false"
}

//vai ao ficheiro users.txt e retorna o segredo

fn get_secret(uid: String, filename: State<PermissionFile>) -> String {
    let file = File::open(filename.0.clone()).unwrap();
    let reader = BufReader::new(file);

    for (_, line) in reader.lines().enumerate() {
        let line = line.unwrap();
        let v: Vec<&str> = line.split(':').collect();

        if v[0] == uid {
            return v[1].to_string();
        }
    }

    "".to_string()
}

//adiciona o user à lista de users permitidos

fn add (uid: String, allowed: State<Allowed>) {
    allowed.users.lock().unwrap().push(uid.clone());
}


//método que começa a autenticação do user
//começa por comparar a chave api
#[post("/has_access", data = "<request>")]
fn has_access(request: Json<Request>, allowed: State<Allowed>) -> &'static str {
    let api_key = "aeS5ieT5hieYoteeVo5Xoo3chahceik9";

    if request.api_key != api_key {
        return "false";
    }

    has_access_to(request, allowed)
}

//Ativado quando o user faz submit do código de autenticação
//compara o código de autenticação com o segredo (no google authenticator) 
#[post("/get_access", data = "<request>")]
fn get_access(request: LenientForm<Request>, allowed: State<Allowed>, filename: State<PermissionFile>) -> Template {
    use google_authenticator::GoogleAuthenticator;
    let auth = GoogleAuthenticator::new();

    let mut context = HashMap::<String, String>::new();
    context.insert("uid".parse().unwrap(), request.uid.parse().unwrap());
    context.insert("url".parse().unwrap(), "http://localhost:8000/get_access".parse().unwrap());

    let secret = get_secret(request.uid.clone(), filename);
    if auth.verify_code(secret.as_str(), request.api_key.as_str(), 1, 0).unwrap() {
        context.insert("done".parse().unwrap(), "true".parse().unwrap());
        add(request.uid.parse().unwrap(), allowed);
    } else {
        context.insert("done".parse().unwrap(), "false".parse().unwrap());
    }

    Template::render("index", context)
}


//Inicia a pagina de inserção do código de acesso
#[get("/<uid>")]
fn home(uid: String) -> Template {
    let mut context = HashMap::<String, String>::new();
    context.insert("uid".parse().unwrap(), uid);
    context.insert("url".parse().unwrap(), "http://localhost:8000/get_access".parse().unwrap());
    context.insert("done".parse().unwrap(), "unknown".parse().unwrap());

    Template::render("index", context)
}

// Página que tem o QR code e o segredo de um novo pedido
#[get("/generate_secret")]
fn generate_secret() -> Template {
    use google_authenticator::GoogleAuthenticator;
    let google_authenticator = GoogleAuthenticator::new();

    let secret = google_authenticator.create_secret(32);
    let qr = google_authenticator.qr_code_url(secret.as_ref(), "2fa-libfuse", "2fa-libfuse", 200, 200, 'M');

    let mut context = HashMap::<String, String>::new();
    context.insert("secret".parse().unwrap(), secret);
    context.insert("qr".parse().unwrap(), qr);

    Template::render("code", context)
}


fn main() {
    rocket::ignite()
        .manage(Allowed { users: Mutex::new(Vec::new()) })
        .mount("/", routes![generate_secret, get_access, home, has_access])
        .attach(Template::fairing())
        .attach(AdHoc::on_attach("Permission File", |rocket| {
            let val = rocket.config().get_string("permission_file").unwrap();
            Ok(rocket.manage(PermissionFile(val)))
        }))
        .launch();
}
